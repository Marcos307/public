/*
 * Tamaño de los diferentes tipos de variables
 * Utilizacion del operador(tiempo de compilacion) "sizeof"
 * 
 * 
 * */

#include <stdio.h>

int main (void){

	int  entero;
	char caracter;
	float real;
	double doble;

	entero = 8;
	caracter = 'A';
	real = 2.55;
	doble = 3.14;

	printf("\nint %d bytes", sizeof(int));
	printf("\nchar %d byte", sizeof(char));
	printf("\nfloat %d bytes", sizeof(float));
	printf("\ndouble %d bytes", sizeof(double));

	printf("\n");

	printf("\nLa variable entero es igual a %d y ocupa %d bytes en memoria", entero, sizeof(entero));
	printf("\nLa variable caracter es igual a %c y ocupa %d byte en memoria", caracter, sizeof(caracter));
	printf("\nLa variable real es igual a %f y ocupa %d bytes en memoria", real, sizeof(real));
	printf("\nLa variable doble es igual a %f y ocupa %d bytes en memoria", doble, sizeof(doble));

	return 0;
}
