# Informatica I

_Repositorio de la materia Informatica I de la Carrera de Electronica de la Universidad Tecnologica Nacional_

## Entorno de Desarrollo 🛠️

_A continuacion se encuentra el listado de las herramientas necesarias para disponer el entorno de desarrollo, algunas de ellas son recomendaciones, que dependera de cada uno de ustedes utilizarlas:_



* [VirtualBox](https://www.virtualbox.org/) - Software de virtualización (opcional)
* [Ubuntu](https://ubuntu.com/) - Sistema Operativo Linux
* [Kate](https://kate-editor.org/) - Editor con Syntax Highlighting
* [gcc](https://gcc.gnu.org/) - The GNU Compiler
* [git](https://git-scm.com/) - Sistema de Control de Versiones
* [GitLab](https://gitlab.frba.utn.edu.ar/) - GitLab UTN

## Buenas Practicas

_Link colaborativo para ir registrando la lista de "Buenas Practicas - A trabajar!"_

	
* [Buenas Practicas Lenguaje C ](https://docs.google.com/document/d/19mOc3SUD3OH_fyS1547eLW-Fr_AQgNzZpofUqMSKPCQ/edit?usp=sharing) - Google Doc Buenas Prácticas






